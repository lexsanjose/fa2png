﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using System.Collections;

namespace Fa2Png
{
    class Program
    {
        static UriBuilder toCrawl;
        static ChromeDriver Driver = new ChromeDriver();


        static void BuildHeaderRequest(HttpWebRequest req)
        {

            req.UserAgent = @"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.ContentType = "text/html; charset=UTF-8";
            req.Headers.Add("Accept-Encoding: gzip, deflate");
            req.Headers.Add("Accept-Language: en-US,en,q=0.5");
        }

        static void DownloadIcon(string imageurl, string downloadpath)
        {
            toCrawl = new UriBuilder(imageurl);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(toCrawl.Uri);
            BuildHeaderRequest(request);

            WebResponse response = request.GetResponse();
            using (Stream inputStream = response.GetResponseStream())
            using (Stream outputStream = File.OpenWrite(downloadpath))
            {
                byte[] buffer = new byte[4096];
                int bytesRead;
                do
                {
                    bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                    outputStream.Write(buffer, 0, bytesRead);
                } while (bytesRead != 0);
            }
            response.Close();
        }

        static void Main(string[] args)
        {
            
            Driver.Navigate().GoToUrl("http://fa2png.io/");
            string size = args[0];
            int wait = Convert.ToInt32(args[1]);
            //string size = "8";
            //int wait = 1000;

            string filename = "{0}.png";
            string[] icons = File.ReadAllText("icons.txt").Split(',');
            string[] colors = File.ReadAllText("colors.txt").Split(',');

            foreach (string color in colors)
            {
                if (!Directory.Exists(Path.Combine(size.ToString(), color)))
                    Directory.CreateDirectory((Path.Combine(size.ToString(), color)));

                foreach (string icon in icons)
                {
                    string downloadpath = Path.Combine(size, color, string.Format(filename, icon));
                    if (!File.Exists(downloadpath))
                    {
                        Driver.FindElement(By.Id("select2-icon-name-input-container")).Click();
                        Driver.FindElement(By.CssSelector("body > span > span > span.select2-search.select2-search--dropdown > input")).Clear();
                        Driver.FindElement(By.CssSelector("body > span > span > span.select2-search.select2-search--dropdown > input")).SendKeys("fa-" + icon);
                        Thread.Sleep(2000);
                        Driver.FindElement(By.CssSelector("#select2-icon-name-input-results > li")).Click();
                        Driver.FindElement(By.Id("icon-color-input")).Clear();
                        Driver.FindElement(By.Id("icon-color-input")).SendKeys("#" + color);
                        Driver.ExecuteScript("$('#icon-size-input').val('')");
                        Driver.FindElement(By.Id("icon-size-input")).SendKeys(size);
                        Driver.FindElement(By.CssSelector("#js-icon-form > button")).Click();
                        Thread.Sleep(wait);
                        DownloadIcon(Driver.FindElement(By.Id("js-icon-download-btn")).GetAttribute("href"), downloadpath);
                    }
                }
            }
            Driver.Quit();
        }
    }
}
