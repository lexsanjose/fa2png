# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

An automated downloader for font-awesome icons to png.
Uses Selenium WebDriver to automate the download steps.

### How do I get set up? ###

1. Once compiled, put the files inside "Put2Debug" to Debug or Release folder
2. Run the program using command prompt
3. First argument is the size of the icon, second argument is the wait time (use for downloading very large size icons)
4. (Optional) Or create a batch file containing both arguments
